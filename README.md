# linux-gtk-projectile-energy

![](screenshot.png)

## dirs

* icon
```bash
cp gtk-pe.png /usr/share/icons/
```
* desktop file
```bash
cp gtk-pe.desktop /home/alarm/.local/share/applications/
```
* executable
```bash
cp gtk-pe /usr/bin
```