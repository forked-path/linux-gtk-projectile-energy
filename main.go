package main

import (
	"flag"
	"fmt"
	"github.com/gotk3/gotk3/gtk"
	"log"
	"math"
	"strconv"
)

const version = "v1.02"

type GUI struct {
	FullScreen bool
	EmSpacing uint
}

type PE struct {
	FPS, Grains, Cal float64
}

func (p *PE) footPoundCal() float64 {
	return (math.Pow(p.FPS, 2) * p.Grains) / 450437 // https://sciencing.com/calculate-bullet-impact-6951380.html
}

func (p *PE) joulesCal() float64 {
	return p.footPoundCal() * 1.3558179483314004 // https://en.wikipedia.org/wiki/Foot-pound_(energy)
	// return .5 * (grains / 15432) * (math.Pow(fps*0.3048, 2))
}

// Hornady Index of Terminal Standards HITS
func (p *PE) hitsCal() float64  {
	return (math.Pow(p.Grains, 2) * p.FPS) / (700000 * math.Pow(p.Cal, 2)) // https://sciencing.com/calculate-bullet-impact-6951380.html
}

func (g *GUI) window() error {
	gtk.Init(nil)
	win, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		return fmt.Errorf("Unable to create window:", err)
	}
	win.SetTitle("projectile energy " + version)
	if _, err := win.Connect("destroy", func() {
		gtk.MainQuit()
	}); err != nil {
		fmt.Println(err)
	}
	grid, err := gtk.GridNew()
	if err != nil {
		return fmt.Errorf("Unable to create grid:", err)
	}
	grid.SetOrientation(gtk.ORIENTATION_VERTICAL)
	labelFps, err := gtk.LabelNew("Enter FPS:")
	if err != nil {
		return fmt.Errorf("Unable to create label:", err)
	}
	entryFps, err := gtk.EntryNew()
	if err != nil {
		return fmt.Errorf("Unable to create entryFps:", err)
	}
	labelGrains, err := gtk.LabelNew("Enter Grains:")
	if err != nil {
		return fmt.Errorf("Unable to create label:", err)
	}
	entryGrains, err := gtk.EntryNew()
	if err != nil {
		return fmt.Errorf("Unable to create entryGrains:", err)
	}
	labelCal, err := gtk.LabelNew("Enter Cal:")
	if err != nil {
		return fmt.Errorf("Unable to create label:", err)
	}
	entryCal, err := gtk.EntryNew()
	if err != nil {
		return fmt.Errorf("Unable to create entryGrains:", err)
	}
	btn, err := gtk.ButtonNewWithLabel("calculate")
	if err != nil {
		return fmt.Errorf("Unable to create button:", err)
	}
	resultsFootPounds, err := gtk.LabelNew("")
	if err != nil {
		return fmt.Errorf("Unable to create label:", err)
	}
	resultsJoules, err := gtk.LabelNew("")
	if err != nil {
		return fmt.Errorf("Unable to create label:", err)
	}
	resultsHits, err := gtk.LabelNew("")
	if err != nil {
		return fmt.Errorf("Unable to create label:", err)
	}
	process := func() {
		floatParse := func(input string) float64 {
			if input == "" {
				return 0
			}
			returnF, err := strconv.ParseFloat(input, 64)
			if err != nil {
				fmt.Println(err)
			}
			return returnF
		}
		cal, err := entryCal.GetText()
		if err != nil {
			fmt.Println(err)
		}
		fps, err := entryFps.GetText()
		if err != nil {
			fmt.Println(err)
		}
		grains, err := entryGrains.GetText()
		if err != nil {
			fmt.Println(err)
		}
		p := PE{
			FPS:    floatParse(fps),
			Grains: floatParse(grains),
			Cal:    floatParse(cal),
		}
		resultsFootPounds.SetText(fmt.Sprintf("%.7f ft/lb", p.footPoundCal()))
		resultsJoules.SetText(fmt.Sprintf("%.7f J", p.joulesCal()))
		resultsHits.SetText(fmt.Sprintf("%.7f HITS", p.hitsCal()))
	}
	if _, err := btn.Connect("clicked", func() {
		process()
	}); err != nil {
		fmt.Println(err)
	}
	grid.Add(labelFps)
	grid.Add(entryFps)
	grid.Add(labelGrains)
	grid.Add(entryGrains)
	grid.Add(labelCal)
	grid.Add(entryCal)
	grid.Add(btn)
	grid.Add(resultsFootPounds)
	grid.Add(resultsJoules)
	grid.Add(resultsHits)
	if g.FullScreen {
		grid.SetRowSpacing(g.EmSpacing)
		grid.SetHAlign(gtk.ALIGN_CENTER)
		win.Maximize()
	} else {
		grid.SetRowSpacing(10)
	}
	win.Add(grid)
	win.ShowAll()
	gtk.Main()
	return nil
}

func main() {
	fs := flag.Bool("full-screen", false, "Full screen mode")
	flag.Parse()
	gui := GUI{
		EmSpacing: 15,
		FullScreen: *fs}
	err := gui.window()
	if err != nil {
		log.Println(err)
	}
}

